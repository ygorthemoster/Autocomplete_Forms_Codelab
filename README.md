# Google Codelab: Autocomplete To Improve Your Forms Codelab
This is an implementation of a Google codelab tutorial, the tutorial is freely avaliable at: https://codelabs.developers.google.com/codelabs/autocomplete/index.html, this is a simple demonstration of the use of autocomplete on html forms, this code is the final product of said tutorial

## Pre-requisites
To test this project you'll need
- [git](https://git-scm.com/)
- A webserver as per tutorial I'll be using the [python http module](https://www.python.org/)

## Installing

All you have to do is clone this repository

```
git clone https://ygorthemoster@gitlab.com/ygorthemoster/Autocomplete_Forms_Codelab.git
```

## Running
You'll have to use a webserver to run this example, if you have python 3 run the following command inside the src folder:

```
python -m http.server 3000
```

or in python 2 run the command:

```
python -m SimpleHTTPServer 3000
```

## License
See [LICENSE](LICENSE)

## Acknowledgments
- Original source by [Ido Green](https://github.com/greenido) available at: https://github.com/greenido/autocomplete-codelab.git
- Tutorial by [Google Codelabs](https://codelabs.developers.google.com/) available at: https://codelabs.developers.google.com/codelabs/autocompleteindex.html
